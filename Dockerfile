FROM golang
RUN apt-get update && apt-get -y upgrade && apt-get -y install wget && apt-get -y install git
CMD ["/bin/bash", "/go/src/bachelor-projekt/scripts/run_main.sh"]
