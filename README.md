# Bachelor projekt

Gitlab repository for our bachelor projekt, which is a remote electroninc voting sytems.
That uses ElGamal encryption, Zero knowledge proof, and secret sharing to be cryptographic secure.
**WE recommend running the system on a linux based system**

Parameters for the system can be changed in the bach.env
****
<h3>Running the program</h3>
<ul>
<li>Ensure docker is running and installed on you computer</li>
<li>Run program docker using, make sure to be in the main project folder <b><i>"bash ./scripts/run_docker.sh"</i></b></li>
<li><b>Options</b>
    <ul>-b to force build</ul>
    <ul>-t to perform tests</ul>
</ul>

<h4>Example</h4>
This will force it to test and build the docker container again:
<blockquote>bash ./scripts/run_docker.sh -b -t</blockquote>

