/*
	The bulletin board is used for printing the results of the election at the end. The board
	prints the result of every decryptor, and each decryptor's signature, so that system users
	can verify the results themselves.
*/

package main

import (
	"bachelor-projekt/helpers"
	"bachelor-projekt/logging"
	"bachelor-projekt/network"
	"bachelor-projekt/packet"
	"encoding/json"
	"fmt"
	"math/big"
	"net/http"
	"os"
	"strings"
)

var Results []helpers.Result

func main() {
	// Fetch RSA sk and pk
	me, _ := network.FindEntityById(os.Args[2])
	_, sk := helpers.GetKeyPair()
	me.Sk = sk

	// Setup listener that listens for packets.
	ln := network.Listen(me)

	// Setup http server that shows election results on HTTP GET
	logging.CriticalMessage("Address of bulletin board: ", "localhost:8080")
	http.HandleFunc("/", handler)
	go http.ListenAndServe(":8080", nil)

	for {
		conn, e := ln.Accept()
		if e != nil {
			panic(e)
		}
		shouldQuit := handleConnection(conn)
		if shouldQuit {
			break
		}
	}

}

/*
	Handles incoming messages. Only cares about packet.ResultToBB
*/
func handleConnection(conn network.AuthenticatedConnection) bool {

	p := conn.ReceivePacket()

	if p.Type == packet.ResultToBB {
		// Takes the input result, and appends it to the result array.
		var result helpers.Result
		e := json.Unmarshal(p.Data, &result)
		sig := big.NewInt(0).SetBytes(p.Signature)
		result.Signature = sig.String()
		result.PacketID = p.ID
		result.Type = p.Type
		if e != nil {
			panic(e)
		}
		appendToResult(result)
		return false
	} else {
		return false
	}

}

/*
	Appends decrypted result to the Results array.
*/
func appendToResult(res helpers.Result) {
	Results = append(Results, res)
}

/*
	Returns all the results and signatures that have been received so far
*/
func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Bulletin board for election \n")
	for i := range Results {
		res := Results[i]
		fmt.Fprintln(w, " ---- "+strings.ToUpper(res.Decryptor)+" ---- ")
		fmt.Fprintln(w, res.Decryptor+" has posted result: "+res.Counter.String()+". Sig: "+res.Signature+"\n"+"Type: "+res.Type+"\n"+"ID: "+res.PacketID)
	}
}
