/*
	Code that runs on the decrypting servers.
	It accepts connections from the tallying server, that sends its results.
	It also accepts connections from other decrypting servers, for when those send their
	c_1^s_i (from here on, this value will be referred to as the 'cipherSecret')
	It stores an array of different cipherSecrets, and once this server wishes to decrypt
	a voteSum, it distributes its ciphersecret to all other decryptors, and then waits until it has enough
	cipherSecrets to decrypt the ciphertext. We keep track of not only the ciphersecrets, but also the c_1 values
	that correspond to the ciphersecret, that is, we store (c_1, c_1^s_i), to ensure that we only use
	the correct secrets.
*/
package main

import (
	"bachelor-projekt/elgamal"
	"bachelor-projekt/helpers"
	"bachelor-projekt/logging"
	"bachelor-projekt/network"
	"bachelor-projekt/packet"
	"bachelor-projekt/polynomials"
	"bachelor-projekt/rsa"
	zkproofs "bachelor-projekt/zk_proofs"
	"encoding/json"
	"fmt"
	"math/big"
	"os"
	"time"
)

// Contains all information about a cipherSecret
type cipherSecret struct {
	VoteCipher *big.Int       // The c_1 value that this cipherSecret can decrypt
	Secret     *big.Int       // The c_1^s_i value.
	SenderID   string         // ID of the decryptor that sent this cipherSecret
	Proof      zkproofs.Proof // Proof that Secret = VoteCipher^s_i
}

// Array of all the cipherSecret acquired from other decryptors so far.
var acquiredCipherSecrets = []cipherSecret{}

func main() {
	// Retrieve signing sk and pk
	_, sk_sign := helpers.GetKeyPair()

	me, _ := network.FindEntityById(os.Args[2])
	me.Sk = sk_sign
	logging.Info(me.Id, "Booted!")

	// Listen for new connections
	ln := network.Listen(me)

	for {
		conn, err := ln.Accept()
		if err != nil {
			// handle error, aka panic
			panic(err)
		}
		handleConnection(conn)
	}
}

func handleConnection(conn network.AuthenticatedConnection) {
	p := conn.ReceivePacket()

	if p.Type == packet.VoteSum && conn.Other.Type == network.Tally {
		// If packet is votesum, receive c1 and c2, and send it to the decryptresult goroutine
		var ciphers elgamal.Ciphertext
		e := json.Unmarshal(p.Data, &ciphers)
		if e != nil {
			panic(e)
		}

		c1, _ := big.NewInt(0).SetString(ciphers.C1.String(), 10)
		c2, _ := big.NewInt(0).SetString(ciphers.C2.String(), 10)

		go decryptresult(c1, c2, conn.Me)
	} else if p.Type == packet.CipherSecret {
		// If packet is a ciphersecret, we check the proof, and then append it to the array of ciphersecrets
		var cSecret cipherSecret
		err := json.Unmarshal(p.Data, &cSecret)
		if err != nil {
			logging.Error(conn.Me.Id, "Failed cipherSecret marshalling with msg: "+err.Error())
		}

		// Fetch the h_i value from trusted dealer.
		hi := fetchHi(conn.Me, cSecret.SenderID)
		pk := getElgamalPublicKey(conn.Me)

		if !zkproofs.VerifyLogProof(cSecret.VoteCipher, cSecret.Secret, hi, pk, cSecret.Proof, cSecret.SenderID) {
			panic("Verification failed.")
		}

		acquiredCipherSecrets = append(acquiredCipherSecrets, cSecret)
	} else {
		// If a packet type could not be found we just ignore it
		fmt.Println("Decryptor couldnt find type of packet.")
	}
}

/*
	Fetch h_i from trusted dealer. This value is equal to g^s_i, and is used in the LOG-proof.
*/

func fetchHi(me network.Entity, senderID string) *big.Int {
	trusted_dealer, _ := network.FindEntityById("trusted_dealer_0")
	conn := network.Connect(me, trusted_dealer)
	conn.SendPacket(senderID, packet.HRequest)
	p := conn.ReceivePacket()

	if p.Type == packet.H {
		var h *big.Int
		json.Unmarshal(p.Data, &h)
		return h
	}
	panic("Didnt receive Hi packet")
}

/*
	Decrypts the result of the election, and sends it to the bulletin board
*/
func decryptresult(c1 *big.Int, c2 *big.Int, me network.Entity) {

	pk := getElgamalPublicKey(me)

	// Compute the shared secret (c_1^s = g^(x*s))
	sharedSecret := computeSecret(me, c1, pk)

	// Use the shared secret to decrypt the election result
	res := elgamal.DecryptUsingSharedSecret(sharedSecret, c1, c2, pk)
	electionResult := discreteLog(&pk.G, res, &pk.P)
	logging.CriticalMessage(me.Id, "Result of election: "+electionResult.String())

	// Finallt, send the result to the bulletin board
	sendResultToBB(me, electionResult)
}

/*
	Helper for retrieving ElGamal pk from the trusted dealer.
*/
func getElgamalPublicKey(me network.Entity) elgamal.Public_key {
	dealer, _ := network.FindEntityById("trusted_dealer_0")
	conn := network.Connect(me, dealer)
	conn.SendPacket([]byte{}, packet.PkRequest)
	p := conn.ReceivePacket()
	var pk elgamal.Public_key

	if p.Type == packet.TDPk {
		json.Unmarshal(p.Data, &pk)
		return pk
	}
	panic("Couldnt retrieve pk")
}

/*
	Computes the value c_1^s from the currently known shares. If not enough are known, the routine waits until it receives more.
*/

func computeSecret(me network.Entity, c1 *big.Int, pk elgamal.Public_key) *big.Int {
	// First compute my own share and distribute it
	share := getShare(me)
	distributeShare(me, share, c1, pk)
	requiredShares := helpers.ReadInt("THRESHOLD", 1)
	var currentShares int
	var relevantShares []cipherSecret

	// Loop over all the collected cipherSecrets, and check if we have enough with voteCipher=c1
	for {
		relevantShares = []cipherSecret{}
		for _, share := range acquiredCipherSecrets {
			if share.VoteCipher.Cmp(c1) == 0 {
				currentShares += 1
				relevantShares = append(relevantShares, share)
			}
		}
		// If enough shares were found, break out
		if len(relevantShares) >= requiredShares {
			break
		}
		// If not, wait 1 second. Hopefully we have received more by then.
		logging.Warning(me.Id, "I dont have enough shares("+fmt.Sprint(len(relevantShares))+") to reconstruct secret.. going to sleep... Shares: ")

		time.Sleep(time.Second * 1)
	}

	// Construct polynomial from the shares
	points := []polynomials.Point{}

	for _, share := range relevantShares {
		x1, ok1 := big.NewInt(0).SetString(share.SenderID[10:], 10)
		x2 := share.Secret
		if !(ok1) {
			logging.Error(me.Id, "Could not interpret shares")
			return big.NewInt(0)
		}
		point := polynomials.Point{X1: big.NewInt(0).Add(x1, big.NewInt(1)), X2: x2}
		points = append(points, point)
	}

	// Compute intersection with the y-axis. This value is equal to c_1^s=g^(x*s) (s is the secret ElGamal key)
	result := polynomials.IntersectionFromPoints(points, &pk.Q, helpers.ReadInt("THRESHOLD", 1)-1)
	return result
}

/*
	Helper for retrieving my own share.
*/
func getShare(me network.Entity) *big.Int {
	dealer, _ := network.FindEntityById("trusted_dealer_0")
	conn := network.Connect(me, dealer)

	conn.SendPacket([]byte{}, packet.ShareRequest)

	p := conn.ReceivePacket()
	if p.Type == packet.SecretShare {
		var shareEncrypted *big.Int
		err := json.Unmarshal(p.Data, &shareEncrypted)
		if err != nil {
			panic(err)
		}
		share := rsa.Decrypt(shareEncrypted, me.Sk)
		logging.Warning(me.Id, "Found share: "+share.String())
		return share
	}
	panic("No share found")
}

/*
	Helper for distributing my own share among all other decryptors.
*/
func distributeShare(me network.Entity, share *big.Int, c1 *big.Int, pk elgamal.Public_key) {
	// First, compute cipherSecret
	cipherShare := big.NewInt(0).Exp(c1, share, &pk.P)
	decryptor_count := helpers.ReadInt("DECRYPT_COUNT", 1)
	for i := 0; i < decryptor_count; i++ {
		decryptor, _ := network.FindEntityById("decryptor_" + fmt.Sprint(i))
		conn := network.Connect(me, decryptor)
		// compute proof for the ciphersecret
		proof := zkproofs.ConstructLogProof(c1, share, pk, me.Id)

		hi := big.NewInt(0).Exp(&pk.G, share, &pk.P)
		// double check that the proof is actually correct..
		if !zkproofs.VerifyLogProof(c1, cipherShare, hi, pk, proof, me.Id) {
			panic("My proof failed! :(")
		}
		cs := cipherSecret{Secret: cipherShare, VoteCipher: c1, SenderID: me.Id, Proof: proof}
		conn.SendPacket(&cs, packet.CipherSecret)
	}
}

// Since the sum of the votes are encoded as g^sum mod p, we have to find the correct value of sum.
// We simply do this by going through every integer, starting at 0, then 1, etc. until we find
// the correct value.

func discreteLog(g *big.Int, n *big.Int, mod *big.Int) *big.Int {
	iter := int64(0)
	for {
		value := big.NewInt(0).Exp(g, big.NewInt(iter), mod)
		if value.Cmp(n) == 0 {
			break
		}
		iter += 1
	}
	return big.NewInt(iter)
}

/*
	Struct for containing a simply log proof.
*/
type Proof struct {
	A0 *big.Int
	A1 *big.Int
	E  *big.Int
	Z  *big.Int
}

/*
	Send the decrypted election result to the bulletin board
*/
func sendResultToBB(me network.Entity, res *big.Int) {
	bb, _ := network.FindEntityById("bulletin_board_0")
	conn := network.Connect(me, bb)

	data := helpers.Result{Counter: res, Decryptor: me.Id}
	conn.SendPacket(&data, packet.ResultToBB)
}
