/*
	Package with ElGamal helper functions and structs
*/
package elgamal

import (
	"bachelor-projekt/helpers"
	"crypto/rand"
	"fmt"
	"math/big"
)

// Generates a pair of keys for El-Gamal. To do this, it needs a safe prime and the
// corresponding Germaine prime. It finds this by searching random large primes until
// it finds one that is Germain.
// It is possible to skip this step by providing a pair of primes in the environment.

func Generate_keys(p *big.Int, q *big.Int) (Public_key, Secret_key) {
	// Check environment for primes to use
	r := rand.Reader

	g := big.NewInt(4)
	x, e := rand.Int(r, q)
	helpers.Error_check(e)
	h := big.NewInt(0)
	h.Exp(g, x, p)

	// Insert the correct values into public and private key structs, that are
	// then returned
	pk := Public_key{P: *p, Q: *q, G: *g, H: *h}
	sk := Secret_key{X: *x}

	fmt.Println(" - Using Following Public Key - ")
	fmt.Println(" p: " + p.String() + "\n q: " + q.String() + "\n g: " + g.String() + "\n h: " + h.String())

	return pk, sk
}

/*
	Decrypts (c1, c2) using the input sk
*/
func Decrypt(c1 *big.Int, c2 *big.Int, pk Public_key, sk Secret_key) *big.Int {
	s_inv := big.NewInt(0)
	s_inv.Exp(c1, big.NewInt(0).Sub(&pk.Q, &sk.X), &pk.P)
	m := big.NewInt(0)
	m.Mod(m.Mul(c2, s_inv), &pk.P)
	return m
}

func DecryptUsingSharedSecret(sharedSecret *big.Int, c1 *big.Int, c2 *big.Int, pk Public_key) *big.Int {
	s_inv := helpers.ModularInverse(sharedSecret, &pk.P)

	return big.NewInt(0).Mod(big.NewInt(0).Mul(s_inv, c2), &pk.P)
}

// Encrypts m under the input public key. We assume that the message has already
// been mapped to the group
func Encrypt(m *big.Int, pk Public_key) (*big.Int, *big.Int, *big.Int) {
	r := rand.Reader
	y, e := rand.Int(r, &pk.P)
	helpers.Error_check(e)

	s := big.NewInt(0)
	s.Exp(&pk.H, y, &pk.P)
	c1 := big.NewInt(0)
	c1.Exp(&pk.G, y, &pk.P)
	c2 := big.NewInt(0)
	c2.Mul(m, s)

	return c1, c2, y
}

// Struct used to represent the public key used in El-Gamal
type Public_key struct {
	P big.Int
	Q big.Int
	G big.Int
	H big.Int
}

// Struct used to represent the secret key used in El-Gamal

type Secret_key struct {
	X big.Int
}

// Struct to contain a ciphertext. Allows easier transmission of encrypted messages over the network

type Ciphertext struct {
	C1 *big.Int
	C2 *big.Int
}
