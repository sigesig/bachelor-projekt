/*
	Packet used for representing the packets we send over the network.
*/
package packet

import (
	"bachelor-projekt/logging"
	"bachelor-projekt/rsa"
	"fmt"
	"math/big"
	"math/rand"
)

// Struct used to wrap around packets sent over the network. Using the 'Type' field,
// we can send packets of any type we wish, we just need to marshal the struct
// we are sending into the 'Data' field.

type GenericPacket struct {
	ID        string
	Type      string
	Data      []byte
	Signature []byte
}

// Signs the packet under the given RSA secret key
func (p *GenericPacket) SignPacket(sk rsa.SecretKey, pk rsa.PublicKey) {
	concatValues := append([]byte(p.ID), append([]byte(fmt.Sprint(p.Type)), p.Data...)...)
	hash := big.NewInt(0).SetBytes(rsa.Hash(concatValues))
	signature := rsa.Decrypt(hash, sk)
	p.Signature = signature.Bytes()
}

// Verifies the packet signature under the given RSA public key
func (p *GenericPacket) ValidateSignature(pk rsa.PublicKey) bool {
	concatValues := append([]byte(p.ID), append([]byte(fmt.Sprint(p.Type)), p.Data...)...)
	hash := big.NewInt(0).SetBytes(rsa.Hash(concatValues))

	reversedSignature := rsa.Encrypt(big.NewInt(0).SetBytes(p.Signature), pk)

	if hash.Cmp(reversedSignature) == 0 {
		return true
	} else {
		logging.Error("Packet", "Observed: "+reversedSignature.String())
		logging.Error("Packet", "Expected: "+hash.String())
		return false
	}
}

// Generates random 63 bit integer used for packet IDs
func GenerateID() int64 {
	return rand.Int63()
}

// Constants used for packet types
const (
	String                = "string"
	PublicKey             = "PublicKey"
	Vote                  = "Vote"
	VoteSum               = "VoteSum"
	Acknowledment         = "Acknowledgement"
	ShareRequest          = "ShareRequest"
	PkRequest             = "PkRequest"
	HRequest              = "HRequest"
	H                     = "H"
	TDPk                  = "TDPk"
	SecretShare           = "SecretShare"
	ShutDown              = "ShutDown"
	DecryptShutDownNotify = "DecryptShutDownNotify"
	Identification        = "Identification"
	CipherSecret          = "CipherSecret"
	DecryptShutDown       = "DecryptShutDown"
	ResultToBB            = "ResultToBB"
	BBRequest             = "BBRequest"
)
