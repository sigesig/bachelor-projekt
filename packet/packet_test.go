package packet

import (
	"bachelor-projekt/rsa"
	"testing"
)

func TestGenericPacket_SignPacket(t *testing.T) {

	pk, sk := rsa.KeyGen(4000)

	packet := GenericPacket{
		ID:        "1",
		Type:      Acknowledment,
		Data:      []byte("Test"),
		Signature: []byte(""),
	}
	packet.SignPacket(sk, pk)
	if !(len(packet.Signature) > 0) {
		t.Errorf("Signature was not properly made")
	}
}
func TestGenericPacket_ValidateSignature(t *testing.T) {
	pk, sk := rsa.KeyGen(2048)

	packet := GenericPacket{
		ID:        "1",
		Type:      Acknowledment,
		Data:      []byte("Test"),
		Signature: []byte(""),
	}
	packet.SignPacket(sk, pk)
	//Test non tampered signature
	if !packet.ValidateSignature(pk) {
		t.Errorf("Should be true")
	}

	packet.Signature = []byte("Rasmus er meget sej")
	//Test tampered signature
	if packet.ValidateSignature(pk) {
		t.Errorf("Should be false")
	}
}
