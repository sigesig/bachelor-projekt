package rsa

import (
	"math/big"
	"testing"
)

var testPk PublicKey
var testSk SecretKey
var msg *big.Int
var plainString []byte

//Setup for tests
func TestMain(m *testing.M) {
	testPk, testSk = KeyGen(2048)
	plainString = []byte("Test besked")
	msg = big.NewInt(0).SetBytes([]byte("Test besked"))
}

func TestEncrypt(t *testing.T) {
	ciphertext := Encrypt(msg, testPk)
	if msg == ciphertext {
		t.Errorf("The ciphertext is the same as the original message")
	}
}

func TestDecrypt(t *testing.T) {
	ciphertext := Encrypt(msg, testPk)
	Decrypt(ciphertext, testSk)
	if msg != ciphertext {
		t.Errorf("The decrypted ciphertext is not the same as the original message")
	}
}

func TestKeyGen(t *testing.T) {
	pk, sk := KeyGen(2048)
	//Basically just check that the n and D have the correct length Because the decrypt with these keys is check in other tests
	lengthOfN := len(pk.N.String())
	lengthOfD := len(sk.D.String())
	if (lengthOfN != 617) || (lengthOfD != 617) {
		t.Errorf("Either N or D is not the correct length")
	}
}

func TestConstructKeys(t *testing.T) {
	newPk, newSk := ConstructKeys(testPk.N, testSk.D)
	if testPk != newPk {
		t.Errorf("The pk are not the same")
	}
	if testSk != newSk {
		t.Errorf("The pk are not the same")
	}
}

// A common choice is to use n = 20; this gives a false positive rate 0.000,000,000,001.
func TestCreatePrime(t *testing.T) {
	p := CreatePrime(2048)
	if !p.ProbablyPrime(20) {
		t.Errorf("Created numer isn not a prime")
	}
}
