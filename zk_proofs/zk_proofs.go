// Package for proving statements about certain things sent over the network.
package zkproofs

import (
	"bachelor-projekt/elgamal"
	"bachelor-projekt/helpers"
	"crypto/sha512"
	"fmt"
	"math/big"
	"strconv"
)

// Constructs proof of knowledge of 'share'. Proves that log_c(c^s_i) = log_g(g^s_i)
func ConstructLogProof(c *big.Int, share *big.Int, pk elgamal.Public_key, userID string) Proof {
	s1 := big.NewInt(0).Exp(&pk.G, helpers.RandomNum(1024), &pk.P)
	a0 := big.NewInt(0).Exp(c, s1, &pk.P)
	a1 := big.NewInt(0).Exp(&pk.G, s1, &pk.P)

	// now get the challenge xor using the hash. we take mod q, because the challenge has to be in the group Z_2^t:
	e := big.NewInt(0).Mod(big.NewInt(0).SetBytes([]byte(HashCommit(c, userID, a0, a1))), &pk.Q)

	// compute proof z1:
	z := big.NewInt(0).Mod(big.NewInt(0).Add(s1, big.NewInt(0).Mul(e, share)), &pk.Q)
	return Proof{A0: a0, A1: a1, E: e, Z: z, UserID: userID}
}

// Verifies that the given log proof is correct
func VerifyLogProof(c *big.Int, ci *big.Int, hi *big.Int, pk elgamal.Public_key, proof Proof, userID string) bool {
	// here we check if the proof is valid. Check that:
	// c^z = a0 * c_i^e
	// g^z = a1 * h_i^e
	p1l := big.NewInt(0).Exp(c, proof.Z, &pk.P)
	p1r := big.NewInt(0).Mod(big.NewInt(0).Mul(proof.A0, big.NewInt(0).Exp(ci, proof.E, &pk.P)), &pk.P)
	p2l := big.NewInt(0).Exp(&pk.G, proof.Z, &pk.P)
	p2r := big.NewInt(0).Mod(big.NewInt(0).Mul(proof.A1, big.NewInt(0).Exp(hi, proof.E, &pk.P)), &pk.P)

	hash := big.NewInt(0).Mod(big.NewInt(0).SetBytes(HashCommit(c, proof.UserID, proof.A0, proof.A1)), &pk.Q)

	return CheckProof(hash, proof.E, "hash") &&
		CheckProof(p1l, p1r, "proof 1") &&
		CheckProof(p2l, p2r, "proof 2") &&
		CheckProof(big.NewInt(0).SetBytes([]byte(userID)), big.NewInt(0).SetBytes([]byte(proof.UserID)), "Checking whether UserID = p.UserID")
}

// ---- The following funcs are used in construction of an OR-proof ---- //

// Takes a fake proof as input and constructs a proof for the voter's actualy vote, using the challenge generated for the fake proof.
func ConstructRealProofFromFakeProof(cipher elgamal.Ciphertext, seed *big.Int, pk elgamal.Public_key, fakeProof Proof, vote int) Proof {
	if vote != 0 && vote != 1 {
		panic("Received incorrect vote value.")
	}
	s := big.NewInt(0).Exp(&pk.G, helpers.RandomNum(1024), &pk.P)
	a0 := big.NewInt(0).Exp(&pk.G, s, &pk.P)
	a1 := big.NewInt(0).Exp(&pk.H, s, &pk.P)

	// now we have commit for both proofs, and thus we would normally send this to Verifier. Instead, we use a hash function
	// to make the proof non-interactive. For now, the hash produces a 1024 bit output
	var xor *big.Int
	if vote == 0 {
		xor = big.NewInt(0).Mod(big.NewInt(0).SetBytes([]byte(HashCommit(cipher.C1, fakeProof.UserID, a0, a1, fakeProof.A0, fakeProof.A1))), &pk.Q)
	} else {
		xor = big.NewInt(0).Mod(big.NewInt(0).SetBytes([]byte(HashCommit(cipher.C1, fakeProof.UserID, fakeProof.A0, fakeProof.A1, a0, a1))), &pk.Q)
	}

	e := big.NewInt(0).Xor(xor, fakeProof.E)

	// compute proof:
	z := big.NewInt(0).Add(s, big.NewInt(0).Mul(e, seed))

	return Proof{A0: a0, A1: a1, E: e, Z: z, UserID: fakeProof.UserID}
}

// Constructs a fake proof for use in an OR-proof. It does this by preselecting a challenge, and then computing commits based on that challenge.
func ConstructFakeProof(cipher elgamal.Ciphertext, seed *big.Int, pk elgamal.Public_key, vote int, userID string) Proof {
	if vote != 0 && vote != 1 {
		panic("Received incorrect vote(" + strconv.Itoa(vote) + "). Should be 0 or 1")
	}
	// First, use a prepicked challenge to construct valid proof for vote=1:
	e := big.NewInt(0).Mod(helpers.RandomNum(1024), &pk.Q) // make sure challenge is mod p
	s := big.NewInt(0).Exp(&pk.G, helpers.RandomNum(1024), &pk.P)
	// define commit. this is calculated using the simulator - it is a proof using the preselected challenge
	// we need two commits: a10 and a11, since we need to prove log_g(c) = log_h(d/g), according to the discrete log proof in sigma.pdf
	a0 := big.NewInt(0).Mul(big.NewInt(0).Exp(&pk.G, s, &pk.P), big.NewInt(0).Exp(cipher.C1, big.NewInt(0).Neg(e), &pk.P))
	var a1 *big.Int

	if vote == 0 {
		a1 = big.NewInt(0).Mul(big.NewInt(0).Exp(&pk.H, s, &pk.P), big.NewInt(0).Exp(big.NewInt(0).Div(cipher.C2, &pk.G), big.NewInt(0).Neg(e), &pk.P))
	} else {
		a1 = big.NewInt(0).Mul(big.NewInt(0).Exp(&pk.H, s, &pk.P), big.NewInt(0).Exp(cipher.C2, big.NewInt(0).Neg(e), &pk.P))
	}

	// we now have commits (a10, a11), challenge (e1), lastly we compute proof (z1). It is simply equal to s1:
	z := s

	return Proof{A0: a0, A1: a1, E: e, Z: z, UserID: userID}
}

func VerifyORProof(p ORProof, cipher elgamal.Ciphertext, pk elgamal.Public_key, userID string) bool {
	// here we check if the proof is valid.
	// g^z0 = a00 * c^e0  ---- These two are related to  ----
	// h^z0 = a01 * d^e0  ---- the proof of vote = 0     ----
	// g^z1 = a10 * c^e1  ---- These two are related to  ----
	// h^z1 = a11 * (d/g)^e1  ---- the proof of vote = 1 ----
	p1l := big.NewInt(0).Exp(&pk.G, p.P0.Z, &pk.P)
	p1r := big.NewInt(0).Mod(big.NewInt(0).Mul(p.P0.A0, big.NewInt(0).Exp(cipher.C1, p.P0.E, &pk.P)), &pk.P)

	p2l := big.NewInt(0).Exp(&pk.H, p.P0.Z, &pk.P)
	p2r := big.NewInt(0).Mod(big.NewInt(0).Mul(p.P0.A1, big.NewInt(0).Exp(cipher.C2, p.P0.E, &pk.P)), &pk.P)

	p3l := big.NewInt(0).Exp(&pk.G, p.P1.Z, &pk.P)
	p3r := big.NewInt(0).Mod(big.NewInt(0).Mul(p.P1.A0, big.NewInt(0).Exp(cipher.C1, p.P1.E, &pk.P)), &pk.P)

	p4l := big.NewInt(0).Exp(&pk.H, p.P1.Z, &pk.P)
	p4r := big.NewInt(0).Mod(big.NewInt(0).Mul(p.P1.A1, big.NewInt(0).Exp(big.NewInt(0).Div(cipher.C2, &pk.G), p.P1.E, &pk.P)), &pk.P)

	// now test whether hashCommit == e0 xor e1
	hash := big.NewInt(0).Mod(big.NewInt(0).SetBytes(HashCommit(cipher.C1, p.UserID, p.P0.A0, p.P0.A1, p.P1.A0, p.P1.A1)), &pk.Q)
	e0_e1_xor := big.NewInt(0).Xor(p.P0.E, p.P1.E)

	verifySuccess := true
	verifySuccess = CheckProof(hash, e0_e1_xor, "Checking whether e0 xor e1 == hash") && verifySuccess

	verifySuccess = CheckProof(p1l, p1r, "Checking g^z0 = a00 * c^e0") && verifySuccess
	verifySuccess = CheckProof(p2l, p2r, "Checking h^z0 = a01 * d^e0") && verifySuccess
	verifySuccess = CheckProof(p3l, p3r, "Checking g^z1 = a10 * c^e1") && verifySuccess
	verifySuccess = CheckProof(p4l, p4r, "Checking h^z1 = a11 * (d/g)^e1") && verifySuccess
	verifySuccess = CheckProof(big.NewInt(0).SetBytes([]byte(userID)), big.NewInt(0).SetBytes([]byte(p.P0.UserID)), "Checking whether UserID = p0.UserID") && verifySuccess
	verifySuccess = CheckProof(big.NewInt(0).SetBytes([]byte(userID)), big.NewInt(0).SetBytes([]byte(p.P1.UserID)), "Checking whether UserID = p1.UserID") && verifySuccess

	return verifySuccess
}

func CheckProof(l *big.Int, r *big.Int, msg string) bool {
	p := false
	if p {
		fmt.Println("\n -----    -----")
		fmt.Println(msg)
		fmt.Println("Left side: " + l.String())
		fmt.Println("Right side: " + r.String())
	}
	if l.Cmp(r) == 0 {
		if p {
			fmt.Println("Partial proof verified\n ")
		}
		return true
	} else {
		fmt.Println("PROOF INCORRECT\n ")
		fmt.Println("Failed for \"" + msg + "\"")
		return false
	}
}

func HashCommit(c *big.Int, id string, commits ...*big.Int) []byte {
	commitString := c.String() + ":" + c.String()
	for _, c := range commits {
		commitString += ":" + c.String()
	}
	commitString += ":" + id
	hash1 := sha512.Sum512([]byte(commitString + ":1"))
	hash2 := sha512.Sum512([]byte(commitString + ":2"))
	res := append(hash1[:], hash2[:]...)
	return res
}

type Proof struct {
	A0     *big.Int
	A1     *big.Int
	E      *big.Int
	Z      *big.Int
	UserID string
}

type ORProof struct {
	P0     Proof
	P1     Proof
	UserID string
}

type CipherAndProof struct {
	Cipher elgamal.Ciphertext
	Proof  ORProof
}
